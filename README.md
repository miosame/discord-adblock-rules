# discord-adblock-rules

If you want to hide "blocked messages" and other indicators that blocked people wrote ("new messages since", ..) but don't have betterdiscord. Install ublock and paste the `rules` file content into custom filters.